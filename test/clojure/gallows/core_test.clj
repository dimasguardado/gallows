(ns gallows.core-test
  (:use clojure.test
        gallows.core))

(def dictionaries (group-by count (slurp-lines "words.txt")))

(defn test-run [word]
  (score word 5 (dictionaries (count word))))

(deftest sample
  (testing "Tests current strategy against sample scores"
    (is (<= (test-run "comaker")  25))
    (is (<= (test-run "cumulate") 9))
    (is (<= (test-run "eruptive") 5))
    (is (<= (test-run "factual") 9))
    (is (<= (test-run "monadism") 8))
    (is (<= (test-run "mus") 25))
    (is (<= (test-run "nagging") 7))
    (is (<= (test-run "oses") 5))
    (is (<= (test-run "remembered") 5))
    (is (<= (test-run "spodumenes") 4))
    (is (<= (test-run "stereoisomers") 2))
    (is (<= (test-run "toxics") 11))
    (is (<= (test-run "trichromats") 5))
    (is (<= (test-run "triose") 5))
    (is (<= (test-run "uniformed") 5))
    (is (<= (run (slurp-lines "sample.txt") 5 dictionaries) (/ 130.0 15)))))