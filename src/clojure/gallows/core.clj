;;   Copyright (c) Dimas Guardado, Jr. All rights reserved.

(ns gallows.core
  (:require [clojure.string :as s]
            [clojure.set :as sets]
            [clojure.tools.cli :as cli])
  (:import [gallows GuessingStrategy HangmanGame Guess GuessWord GuessLetter HangmanGame$Status]
           [com.yammer.metrics Metrics]
           [com.yammer.metrics.core Timer])
  (:gen-class))

(defn slurp-lines
  "Convenience function. Converts newline separated text into a vecter of strings"
  [source]
  (s/split-lines (slurp source)))

(defn guess-pattern
  "Returns a regular expression pattern which matches words that with letters in the given hangman positions"
  [known guessed]
  (let [position-pattern (if (empty? guessed) "[A-Z]" (str "[^" (apply str guessed) \]))
        pattern (s/replace known (re-pattern (str HangmanGame/MYSTERY_LETTER)) position-pattern)]
    (re-pattern pattern)))

(defn guessed-letters
  "Returns the set of all letters which have already been guessed"
  [^HangmanGame game]
  (sets/union (apply hash-set (.getCorrectlyGuessedLetters game))
              (apply hash-set (.getIncorrectlyGuessedLetters game))))

(defn matches-known-fn
  "Returns a function which matches words wich are candidates given known information in a hangman game"
  [known letters]
  (let [pattern (guess-pattern known letters)]
    #(re-matches pattern  %)))

(defn cache-key
  "Returns an immutable map representation of a Hangman game in specific state.
   Used to cache filtered dictionaries and letter distributions for those states"
  [^HangmanGame game]
  {:known (.getGuessedSoFar game)
   :letters (guessed-letters game)
   :words (apply hash-set (.getIncorrectlyGuessedWords game))})

(defn collapse
  "Returns a new dictionary of best possible candidates given known information in a hangman game"
  [dictionary cache-key]
  (let [{:keys [known letters words]} cache-key
        matches-known? (matches-known-fn known letters)
        not-wrong-word? #(not (words %))]
    (->> dictionary
      (map s/upper-case)
      (filter (every-pred matches-known? not-wrong-word?)))))

(defn distribution
  "Returns a map from the all the letters to the count of their occurrences in the dictionary.
   Multiple occurrences in a given word are only counted once."
  [dictionary cache-key]
  (let [{:keys [letters]} cache-key]
    (->>
      dictionary
      (map #(into #{} %))
      (apply concat)
      (remove letters)
      frequencies)))

(defn dict-memo 
  "Returns a memoized version of f, ignoring the dictionary argument, using cache-key as its stored value lookup"
  [f]
  (let [cache (atom {})]
    (fn [dictionary cache-key]    
      (let [cached (@cache cache-key)]
        (or cached ((swap! cache assoc cache-key (f dictionary cache-key)) cache-key))))))

(def mem-collapse (dict-memo collapse))

(def mem-distribution (dict-memo distribution))

(defn next-guess
  "Returns a letter guess representing the most likely letter to appear given the word length and previous guesses,
   or a word guess if a single word in the dictionary would be unambiguously correct."
  [dictionary ^HangmanGame game]
  (let [cache-key (cache-key game)
        dictionary (mem-collapse dictionary cache-key)
        freq (mem-distribution dictionary cache-key)
        next (key (reduce (partial max-key val) freq))
        project (filter (fn [^String s] (.contains s (str next))) dictionary)]
    (if (= (count project) 1)
      (GuessWord. (first project))
      (GuessLetter. next))))

 ;; Given that the test rig is entirely in Clojure, this type is more of an homage to the original assignment than 
 ;; actually useful. When AOT compiled this type can (potentially) be used as a direct implemenation of GuessingStrategy.
(deftype ProbabilisticGuesser [dictionary]
  GuessingStrategy
  (nextGuess [this game] (next-guess (.dictionary this) game)))

(defn play
  "Plays the given game of hangman with the given guesser. Returns the game in a completed state."
  [^HangmanGame game ^GuessingStrategy guesser]
  (loop [^Guess guess (.nextGuess guesser game)]
    ;(println (str game))
    ;(println (str guess))
    (.makeGuess guess game)
    (if (= (.gameStatus game) HangmanGame$Status/KEEP_GUESSING)
      (recur (.nextGuess guesser game))))
  (println (str game))
  game)

(defn score
  "Returns the score of playing a hangman game against the probabilistic guesser"
  [word max-guesses dictionary]
  (let [^HangmanGame game (HangmanGame. word max-guesses)
        ^GuessingStrategy guesser (ProbabilisticGuesser. dictionary)
        ^HangmanGame outcome (play game guesser)]
    (.currentScore outcome)))

(defn run
  "Plays each word agianst the probabilistic guesser and returns the average score per game"
  [words max-guesses dictionaries]
  (let [timer (Metrics/newTimer HangmanGame "play-time")
        hist (Metrics/newHistogram HangmanGame "score" false)]
    (doseq [word words]
      (let [dictionary (dictionaries (count word))]
        (.update hist ^long (.time timer #(score word max-guesses dictionary)))))
    (println "Average time per game" (.mean timer))
    (.mean hist)))

(defn run-batch
  "Given a path to a dictionary file, sets up a series of hangman runs according to run-mode, word list, and game options"
  [dict-file max-guesses benchmark words]
  (let [dictionary (slurp-lines dict-file)
        words (cond
                benchmark (take 1000 (distinct (repeatedly #(rand-nth dictionary))))
                (not (empty? words)) words
                :default (slurp-lines *in*))
        score (run words max-guesses (group-by count dictionary))]
    (println "Average Score per Word: " score)))

(defn -main
  "Runs a series of hangman games"
  [& args]
  (let [[options words banner]
        (cli/cli args
                 ["-d" "--dictionary" "The Location of a dictionary file used to power the guesser" :default "words.txt"]
                 ["-g" "--max-guesses" "The maximum number of incorrect letter/word guesses before the convict is hung (the game is lost)" 
                  :parse-fn #(Integer/valueOf %) :default 5]
                 ["-b" "--benchmark" "Run in Benchmark mode" :default false :flag true]
                 ["-h" "--help" "Show help" :default false :flag true])
        {:keys [dictionary max-guesses benchmark help]} options]
    (when help
      (println banner)
      (println "Word list may either be fed in to the command directly or via STDIN as a newline separated list of words")
      (System/exit 0))
    (run-batch dictionary max-guesses benchmark words)))
