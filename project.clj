(defproject humandriven/gallows "0.1.0-SNAPSHOT"
  :description "An artificially intelligent Hangman player"
  :url "http://github.com/dguardado/gallows"
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [org.clojure/tools.cli "0.2.2"]
                 [com.yammer.metrics/metrics-core "2.1.2"]]
  :source-paths ["src/clojure"]
  :java-source-paths ["src/java"]
  :test-paths ["test/clojure"]
  :jar-name "gallows-lib.jar"
  :uberjar-name "gallows.jar"
  :main gallows.core)
