# Gallows

Gallows is an artificially intelligent Hangman player written in Clojure, with some rigging to run 
the agent from the command line. It is not terribly clever, but attempts to guess the next most
likely letter, taking into account previous guesses.

## Usage

Gallows is built using the latest preview of Leiningen 2. Instructions for installing Leiningen
can be found here: https://github.com/technomancy/leiningen

Once Leiningen is installed, the application can be built using the uberjar command

    $ lein uberjar

The application can now be found in target/gallows.jar

Once built, the application can be run with the following command

    $ java -jar gallows.jar [args]

## Options

The Gallows app accepts the following options:

-d --dictionary  - Location of a dictionary file to power the guessing agent (defaults to "words.txt" in the current directory)

-g --max-guesses - The maximum number of incorrect guesses before failure (defaults to 5)

-b --benchmark   - Run in benchmark mode (will run the game against 1000 random words from the provided dictionary)

-h --[no-]help   - Prints usage information (defaults to false)

The application also expects a list of words to play as input, which can be specified on the command, or piped into STDIN
as a newline-separated

## Examples

A Gallows run with the default dictionary and guesses, with command-line specified words would look like:

    $ java -jar gallows.jar try these words

A run with dictionary and guesses specified and play words piped into STDIN from a file would look like

    $ java -jar gallows.jar -d /path/to/dictionary.txt -g 5 < sample.txt

A benchmark run

    $ java -jar gallows.jar -d /path/to/dictionary.txt -b

## License

Java code was provided (though slightly modified), source unknown.

All other text/code Copyright © Dimas Guardado, Jr. unless otherwise specified
